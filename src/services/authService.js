const config = require("../config");
console.log(config.oauthUrl)
function redirectUri() {
    return `${config.oauthUrl}/authorize?client_id=${config.clientId}`;
  }

module.export = {
    redirectUri: redirectUri
}
